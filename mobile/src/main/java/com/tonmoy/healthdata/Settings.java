package com.tonmoy.healthdata;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.tonmoy.healthdata.model.AppSettings;
import com.tonmoy.healthdata.utility.UserCachedData;


public class Settings extends Activity implements OnClickListener {
	Spinner updateFrequencySpinner;
	// Spinner pollTimeSpinner;
	Button save_btn, return_btn, about_btn;
	EditText url_edt, rover_id_edt, logfile_loc_edt, logfile_pref_edt;
	RadioButton gps_yes, data_transmit_yes, local_storage_yes;
	RadioButton gps_no, data_transmit_no, local_storage_no;
//	CheckBox realtime_checkbox;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_activity);
		initialize();
	}

	private void initialize() {
		save_btn = (Button) findViewById(R.id.buttonSave);
		return_btn = (Button) findViewById(R.id.buttonReturn);
		about_btn = (Button) findViewById(R.id.buttonAbout);
		save_btn.setOnClickListener(this);
		return_btn.setOnClickListener(this);
		about_btn.setOnClickListener(this);

		// spinner
		updateFrequencySpinner = (Spinner) findViewById(R.id.spinnerUpdateFrequency);
		// pollTimeSpinner = (Spinner) findViewById(R.id.spinnerPollTimes);

		// Check Box
//		realtime_checkbox = (CheckBox) findViewById(R.id.checkBoxRealtime);
		// EditText
//		url_edt = (EditText) findViewById(R.id.editTextUrl);
//		rover_id_edt = (EditText) findViewById(R.id.editTextRoverID);
//		logfile_loc_edt = (EditText) findViewById(R.id.editTextLogfileLocation);
//		logfile_pref_edt = (EditText) findViewById(R.id.editTextLogfilePrefix);
//
//		// RadioButton
//		gps_yes = (RadioButton) findViewById(R.id.gps_yes);
//		data_transmit_yes = (RadioButton) findViewById(R.id.data_transmit_yes);
//		local_storage_yes = (RadioButton) findViewById(R.id.local_storage_yes);
//
//		gps_no = (RadioButton) findViewById(R.id.gps_no);
//		data_transmit_no = (RadioButton) findViewById(R.id.data_transmit_no);
//		local_storage_no = (RadioButton) findViewById(R.id.local_storage_no);

		if (UserCachedData.getCachedData(this).app_settings == null) {
			saveData();
		} else {
			initializedValues();
		}

	}

	private void initializedValues() {
		// TODO Auto-generated method stub
		AppSettings app_settings = UserCachedData.getCachedData(this).app_settings;
		updateFrequencySpinner
				.setSelection(((ArrayAdapter<String>) updateFrequencySpinner
						.getAdapter()).getPosition(app_settings.updateFrequency
						+ ""));

		// updateFrequencySpinner.set;
		gps_yes.setChecked(app_settings.isGPSOnly);
		gps_no.setChecked(!app_settings.isGPSOnly);
		data_transmit_yes.setChecked(app_settings.dataTransmitEnabled);
		data_transmit_no.setChecked(!app_settings.dataTransmitEnabled);
//		realtime_checkbox.setChecked(app_settings.isRealtime);
		local_storage_yes.setChecked(app_settings.isLocationStorageEnabled);
		local_storage_no.setChecked(!app_settings.isLocationStorageEnabled);
		url_edt.setText(app_settings.url);
		rover_id_edt.setText(app_settings.rover_id);
		// pollTimeSpinner.setSelection(((ArrayAdapter<String>) pollTimeSpinner
		// .getAdapter()).getPosition(app_settings.polltime + ""));
		logfile_loc_edt.setText(app_settings.file_location);
		logfile_pref_edt.setText(app_settings.file_prefix);

	}

	@Override
	public void onClick(View v) {
		if (v == save_btn) {
			saveData();

		} else if (v == return_btn) {
			finish();
		} else if (v == about_btn) {
			//startActivity(new Intent(this, AboutActivity.class));
//			Toast.makeText(
//					this,
//					"Developed by Md. Monsur Hossain Tonmoy\n eLITEs\n BUET 07",
//					Toast.LENGTH_LONG).show();
		}

	}

	private void saveData() {
		JSONObject js = new JSONObject();
		try {
			js.put(AppSettings.UPDATE_FRQUENCY,
					updateFrequencySpinner.getSelectedItem());
			js.put(AppSettings.GPS_ONLY, gps_yes.isChecked());
			js.put(AppSettings.TRANSMIT_DATA, data_transmit_yes.isChecked());
			js.put(AppSettings.URL, url_edt.getText().toString());
			js.put(AppSettings.ROVER_ID, rover_id_edt.getText().toString());
			// js.put(AppSettings.POLL_TIME, pollTimeSpinner.getSelectedItem());
//			js.put(AppSettings.IS_REALTIME, realtime_checkbox.isChecked());
			js.put(AppSettings.LOCAL_STORAGE_ENABLE,
					local_storage_yes.isChecked());
			js.put(AppSettings.FILE_LOCATION, logfile_loc_edt.getText()
					.toString());
			js.put(AppSettings.FILE_PREFIX, logfile_pref_edt.getText()
					.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UserCachedData.saveCachedData(this, js.toString());
		Log.d("tonmoy", js.toString());
		Toast.makeText(this, "Settings Saved",Toast.LENGTH_LONG).show();
		finish();

	}

}
