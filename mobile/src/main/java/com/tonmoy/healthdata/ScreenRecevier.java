package com.tonmoy.healthdata;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ScreenRecevier extends BroadcastReceiver {

	private boolean screenOff;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
			screenOff = true;
		} else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
			screenOff = false;
		}
		if (LocationService.isRunning()) {
			Intent i = new Intent(context, LocationService.class);
			i.putExtra("screen_state", screenOff);
			context.startService(i);
		}

	}
	/*
	 * Developed by Monsur Hossain Tonmoy eLITEs BUET 07
	 */
}