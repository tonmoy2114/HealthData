package com.tonmoy.healthdata.model;

/**
 * Created by hossaim3 on 6/28/2017.
 */

public class HealthInfo {
    public String heartRateValue = "";
    public String heartAccuracy = "";
    public String lightValue = "";
    public String stepCountValue = "";
    public String timestamp = "";

}
