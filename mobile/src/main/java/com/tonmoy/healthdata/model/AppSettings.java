package com.tonmoy.healthdata.model;

import org.json.JSONObject;

public class AppSettings {
	public static String UPDATE_FRQUENCY = "update_frequency";
	public static String GPS_ONLY = "gps_only";
	public static String TRANSMIT_DATA = "transmit_data";
	public static String URL = "url";
	public static String ROVER_ID = "ID";
//	public static String POLL_TIME = "poll_time";
	public static String IS_REALTIME = "is_realtime";
	public static String LOCAL_STORAGE_ENABLE = "local_storage_enable";
	public static String FILE_LOCATION = "file_location";
	public static String FILE_PREFIX = "file_prefix";
	public static String FILE_PREFIX_HealthData = "file_prefix_health";
	public static String FILE_PREFIX_PPG = "file_prefix_ppg";

	public int updateFrequency, polltime;
	public boolean isGPSOnly;
	public boolean dataTransmitEnabled;
	public boolean isGet, isLocationStorageEnabled;
//	public boolean isRealtime;

	public String url, rover_id, file_location, file_prefix,file_prefix_healthdata,file_prefix_ppg;

	public AppSettings(JSONObject jObj) {
		updateFrequency = jObj.optInt(UPDATE_FRQUENCY);
		isGPSOnly = jObj.optBoolean(GPS_ONLY, true);
		dataTransmitEnabled = jObj.optBoolean(TRANSMIT_DATA, true);
//		polltime = jObj.optInt(POLL_TIME);
//		isRealtime = jObj.optBoolean(IS_REALTIME, false);
		isLocationStorageEnabled = jObj.optBoolean(LOCAL_STORAGE_ENABLE, true);

		url = jObj.optString(URL);
		rover_id = jObj.optString(ROVER_ID);
		file_location = jObj.optString(FILE_LOCATION);
		file_prefix = jObj.optString(FILE_PREFIX);
		file_prefix_healthdata = jObj.optString(FILE_PREFIX_HealthData);
		file_prefix_ppg = jObj.optString(FILE_PREFIX_PPG);
	}

}
