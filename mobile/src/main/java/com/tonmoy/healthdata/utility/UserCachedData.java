package com.tonmoy.healthdata.utility;

import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.tonmoy.healthdata.model.AppSettings;


public class UserCachedData {
	private static UserCachedData userData = null;
	public AppSettings app_settings = null;
	private String api_key;
	private String remindMeLater = null;
	Context context;

	private UserCachedData(Context ct) {
		context = ct;
		readSavedUserData();
	}

	public static synchronized UserCachedData getCachedData(Context ct) {

		if (userData == null) {
			userData = new UserCachedData(ct);
		}

		return userData;
	}

	public static synchronized void saveCachedData(Context ct, String data) {

		SharedPreferences sharedPref = ct.getSharedPreferences(
				"com.tonmoy.locationservice.utility", 0);

		try {
			Editor editor = sharedPref.edit();
			editor.putString("UserData", data);
			editor.commit();
			userData = null;
			getCachedData(ct);

		} catch (Exception e) {
			Log.d("Error", e.toString());
		}
	}

	public void DeleteSession() {
		userData = null;
		app_settings = null;
		SharedPreferences sharedPref = context.getSharedPreferences(
				"com.tonmoy.locationservice.utility", 0);
		Editor editor = sharedPref.edit();
		editor.clear();
		editor.commit();

	}

	public void readSavedUserData() {

		SharedPreferences sharedPref = context.getSharedPreferences(
				"com.tonmoy.locationservice.utility", 0);

		try {
			String data = sharedPref.getString("UserData", null);

			if (data != null) {
				this.app_settings = new AppSettings(new JSONObject(data));
			} else {
				this.app_settings = null;

			}

		} catch (Exception e) {
			Log.d("Error", e.toString());
		}
	}

}
