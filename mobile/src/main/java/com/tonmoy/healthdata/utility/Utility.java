package com.tonmoy.healthdata.utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import com.tonmoy.healthdata.model.AppSettings;

import org.json.JSONObject;

public class Utility {
    public static int filesize = 1024 * 1024;

    public static File createNewFile(Context ct, String filePrefix) {
        File file = null;
        SharedPreferences sharedPref = ct.getSharedPreferences(
                "com.tonmoy.locationservice.utility", 0);

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String newFileName = filePrefix + timeStamp + ".csv";

        try {
            String data = sharedPref.getString("UserData", null);
            if (data != null) {
                JSONObject js = new JSONObject(data);
                js.put(AppSettings.FILE_PREFIX, newFileName);
                UserCachedData.saveCachedData(ct, js.toString());
                file = Utility.getOutputFile(ct, newFileName);
            }

        } catch (Exception e) {
            Log.d("Error", e.toString());
        }
        return file;
    }

    public static File getOutputFile(Context ct) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.


        File storageDir = new File(Environment.getExternalStorageDirectory()
                + File.separator
                + UserCachedData.getCachedData(ct).app_settings.file_location);

        // Create the storage directory if it does not exist
        if (!storageDir.exists()) {
            if (!storageDir.mkdirs()) {
                Log.d("tonmoy", "failed to create directory");
                return null;
            }
        }
        String filename = storageDir.getPath() + File.separator
                + UserCachedData.getCachedData(ct).app_settings.file_prefix;
        File gpsDataFile = new File(filename);
        if (!gpsDataFile.exists()) {
            try {
                gpsDataFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        // String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
        // .format(new Date());
        // File dataFile = new File(storageDir.getPath() + File.separator
        // + "gpsdata." + timeStamp + ".txt");

        return gpsDataFile;
    }

    public static File getOutputFile(Context ct, String fileNameWithSalt) {
        File storageDir = new File(Environment.getExternalStorageDirectory()
                + File.separator
                + UserCachedData.getCachedData(ct).app_settings.file_location);
        if (!storageDir.exists()) {
            if (!storageDir.mkdirs()) {
                Log.d("tonmoy", "failed to create directory");
                return null;
            }
        }
        String filename = storageDir.getPath() + File.separator
                + fileNameWithSalt;
        File gpsDataFile = new File(filename);
        if (!gpsDataFile.exists()) {
            try {
                gpsDataFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return gpsDataFile;
    }

    public static void writeStringAsFile(final String data, File file,
                                         Context context) {
        try {
            BufferedWriter bW;
            bW = new BufferedWriter(new FileWriter(file, true));
            bW.write(data);
            bW.newLine();
            bW.flush();
            bW.close();
        } catch (IOException e) {
        }
    }

}
