package com.tonmoy.healthdata.questionnaire;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tonmoy.healthdata.R;

public class Question1Fragment extends Fragment {
    private View view;
    private RadioGroup options;
    private String answer;

    public Question1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_question1, container, false);

        return view;
    }

    private void getChoice(int id) {
        RadioButton selected = (RadioButton) view.findViewById(id);
        answer = (String) selected.getText();
    }
}
