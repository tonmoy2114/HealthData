package com.tonmoy.healthdata.questionnaire;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.tonmoy.healthdata.R;
public class Question3Fragment extends Fragment {
    private View view;
    private RadioGroup options;
    private String answer;

    public Question3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_question3, container, false);

        options = (RadioGroup) view.findViewById(R.id.payoffGroup);

        options.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {
                    case R.id.radioOption1:
                        getChoice(R.id.radioOption1);
                        break;
                    case R.id.radioOption2:
                        getChoice(R.id.radioOption2);
                        break;
                    case R.id.radioOption3:
                        getChoice(R.id.radioOption3);
                        break;
                    case R.id.radioOption4:
                        getChoice(R.id.radioOption4);
                        break;
                }
            }
        });
        return view;
    }

    private void getChoice(int id){
        RadioButton selected = (RadioButton) view.findViewById(id);
        answer = (String) selected.getText();
    }
}
