package com.tonmoy.healthdata.questionnaire;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.tonmoy.healthdata.MainActivity;
import com.tonmoy.healthdata.R;


public class QuestionnaireActivity extends AppCompatActivity implements View.OnClickListener{

    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private static final int NUM_PAGES = 5;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;
    Button buttonSkip,buttonNext;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);

        Log.d("tonmoy", System.currentTimeMillis()+"");

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        buttonSkip=(Button)findViewById(R.id.buttonSkip);
        buttonNext=(Button)findViewById(R.id.buttonNext);
        buttonSkip.setOnClickListener(this);
        buttonNext.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    @Override
    public void onClick(View view) {
        if(view==buttonSkip)
        {
            if(mPager.getCurrentItem()<3) {
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
            }
        }
        else if (view==buttonNext)
        {
            if(mPager.getCurrentItem()<3) {
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
            }

            else
            {
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }
        }
        else {

        }
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment=new Question1Fragment();
            switch (position){
                case 0:
                    fragment=new Question1Fragment();
                    break;
                case 1:
                    fragment=new Question2Fragment();
                    break;
                case 2:
                    fragment=new Question3Fragment();
                    break;
                case 3:
                    fragment=new Question4Fragment();
                    break;
                default:
                    break;
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

    }
    }
