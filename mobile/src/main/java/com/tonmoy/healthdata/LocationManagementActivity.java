package com.tonmoy.healthdata;

import android.app.Activity;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tonmoy.healthdata.model.AppSettings;
import com.tonmoy.healthdata.utility.UserCachedData;
import com.tonmoy.healthdata.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class LocationManagementActivity extends Activity implements ServiceConnection,
        OnClickListener {

    int current_state;
    int STATE_START = 1, STATE_STOP = 2;
    private Messenger mServiceMessenger = null;
    private final Messenger mMessenger = new Messenger(
            new IncomingMessageHandler());

    boolean mIsBound;

    private ServiceConnection mConnection = this;
    TextView tv, roverID_txt, unixtime_txt, latitude_txt, longitude_txt,
            altitude_txt, speed_txt, accuracy_txt;
    Button start_btn, copy_btn, stop_btn, settings_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_management);
        initialized();
        tv = (TextView) findViewById(R.id.textViewLocation);
        automaticBind();
    }


    private void initialized() {
        roverID_txt = (TextView) findViewById(R.id.textViewRoverId);
        unixtime_txt = (TextView) findViewById(R.id.textViewUnixtime);
        latitude_txt = (TextView) findViewById(R.id.textViewLatitude);
        longitude_txt = (TextView) findViewById(R.id.textViewLongitude);
        altitude_txt = (TextView) findViewById(R.id.textViewAltitude);
        speed_txt = (TextView) findViewById(R.id.textViewSpeed);
        accuracy_txt = (TextView) findViewById(R.id.textViewAccuracy);

        start_btn = (Button) findViewById(R.id.buttonStart);
        copy_btn = (Button) findViewById(R.id.buttonCopy);
        stop_btn = (Button) findViewById(R.id.buttonStop);
        settings_btn = (Button) findViewById(R.id.buttonSettings);

        start_btn.setOnClickListener(this);
        copy_btn.setOnClickListener(this);
        stop_btn.setOnClickListener(this);
        settings_btn.setOnClickListener(this);

        if (UserCachedData.getCachedData(this).app_settings == null) {
            saveDefaultSettings();
        }

    }

    private void saveDefaultSettings() {
        // TODO Auto-generated method stub
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String newFileName = "Location_Data" + timeStamp + ".txt";
        String newHealthFileName = "Health_Data" + timeStamp + ".txt";
        String newPPGFileName = "PPG_Data" + timeStamp + ".txt";
        JSONObject js = new JSONObject();
        try {
            js.put(AppSettings.UPDATE_FRQUENCY, 1);
            js.put(AppSettings.GPS_ONLY, true);
            js.put(AppSettings.TRANSMIT_DATA, false);
            js.put(AppSettings.URL, "www.example.com");
            js.put(AppSettings.ROVER_ID, "rover001");
            // js.put(AppSettings.POLL_TIME, 10);
            js.put(AppSettings.IS_REALTIME, false);
            js.put(AppSettings.LOCAL_STORAGE_ENABLE, true);
            js.put(AppSettings.FILE_LOCATION, "gpslog");
            js.put(AppSettings.FILE_PREFIX, newFileName);
            js.put(AppSettings.FILE_PREFIX_HealthData, newHealthFileName);
            js.put(AppSettings.FILE_PREFIX_PPG, newPPGFileName);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        UserCachedData.saveCachedData(this, js.toString());

    }

    /*
     * Developed by Monsur Hossain Tonmoy eLITEs BUET 07
     */
    private void automaticBind() {
        if (LocationService.isRunning()) {
            tv.setText("Searching Location...");
            doBindService();
            current_state = STATE_START;
        } else {
            current_state = STATE_STOP;
        }
        changeButtonState(current_state);
    }

    private void changeButtonState(int state) {
        if (state == STATE_START) {
            start_btn.setEnabled(false);
            stop_btn.setEnabled(true);
            copy_btn.setEnabled(true);
        } else if (state == STATE_STOP) {
            start_btn.setEnabled(true);
            stop_btn.setEnabled(false);
            copy_btn.setEnabled(false);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            doUnbindService();
        } catch (Throwable t) {
            Log.e("failed", "Failed to unbind from the service", t);
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mServiceMessenger = new Messenger(service);
        try {
            Message msg = Message.obtain(null,
                    LocationService.MSG_REGISTER_CLIENT);
            msg.replyTo = mMessenger;
            mServiceMessenger.send(msg);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even do
            // anything with it
        }

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mServiceMessenger = null;
    }

    private class IncomingMessageHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            // Log.d(LOGTAG,"IncomingHandler:handleMessage");
            switch (msg.what) {
                // case LocationService.MSG_SET_INT_VALUE:
                // //textIntValue.setText("Int Message: " + msg.arg1);
                // break;
                case LocationService.MSG_SET_LAT_LONG_VALUE:
                    Location loc = msg.getData().getParcelable("location");
                    String lat = loc.getLatitude() + "";
                    String longi = loc.getLongitude() + "";
                    tv.setText("lat is " + lat + "long is : " + longi);
                    showUpdatedLocationInfo(loc);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }

    }

    private void showUpdatedLocationInfo(Location loc) {
        tv.setText("Location Found.");
        unixtime_txt.setText(System.currentTimeMillis() / 1000L + "");
        latitude_txt.setText(loc.getLatitude() + "");
        longitude_txt.setText(loc.getLongitude() + "");
        altitude_txt.setText(loc.getAltitude() + "");
        speed_txt.setText(loc.getSpeed() + "");
        accuracy_txt.setText(loc.getAccuracy() + "");
    }

    /**
     * Bind this Activity to MyService
     */
    private void doBindService() {
        bindService(new Intent(this, LocationService.class), mConnection,
                Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    /**
     * Un-bind this Activity to MyService
     */
    private void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with it,
            // then now is the time to unregister.
            if (mServiceMessenger != null) {
                try {
                    Message msg = Message.obtain(null,
                            LocationService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mServiceMessenger.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has
                    // crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    @Override
    public void onClick(View v) {
        if (v == start_btn) {
            tv.setText("Searching Location...");
            writeData("log started");
            Intent i = new Intent(this, LocationService.class);
            startService(i);
            doBindService();
            current_state = STATE_START;
            changeButtonState(current_state);
            Log.d("tonmoy", "started Service");
            // start_btn.set
        }
        if (v == stop_btn) {
            tv.setText("");
            writeData("log stopped");
            Intent i = new Intent(this, LocationService.class);
            stopService(i);
            doUnbindService();
            current_state = STATE_STOP;
            changeButtonState(current_state);
            // start_btn.set
        }
        if (v == settings_btn) {
            Intent i = new Intent(this, Settings.class);
            // startActivity(i);
            //getAppUsageHistory();
        }

        if (v == copy_btn) {
            ClipboardManager ClipMan = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

            if (latitude_txt.getText().length() > 0) {
                String info = "Latitude : " + latitude_txt.getText()
                        + " Longitude : " + longitude_txt.getText();
                ClipMan.setText(info);
                Toast.makeText(this, "Current location copied to clipboard", Toast.LENGTH_LONG)
                        .show();
            }
        }

    }

    private void writeData(String data) {
        File currentFile = Utility.getOutputFile(this);
        Utility.writeStringAsFile(data, currentFile, this);
    }

    void getAppUsageHistory(int day) {
        final UsageStatsManager usageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);// Context.USAGE_STATS_SERVICE);
        Calendar beginCal = Calendar.getInstance(Locale.getDefault());
        beginCal.set(Calendar.HOUR_OF_DAY, 19);
        beginCal.set(Calendar.MINUTE, 0);
        beginCal.set(Calendar.SECOND, 0);
        beginCal.set(Calendar.DAY_OF_MONTH, day);
        beginCal.set(Calendar.MONTH, 6);
        beginCal.set(Calendar.YEAR, 2017);

        Calendar endCal = Calendar.getInstance(Locale.getDefault());
        endCal.set(Calendar.HOUR_OF_DAY, 19);
        endCal.set(Calendar.MINUTE, 59);
        endCal.set(Calendar.SECOND, 59);
        endCal.set(Calendar.DAY_OF_MONTH, day+4);
        endCal.set(Calendar.MONTH, 6);
        endCal.set(Calendar.YEAR, 2017);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final List<UsageStats> queryUsageStats = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, beginCal.getTimeInMillis(), endCal.getTimeInMillis());
            System.out.println("results for " + beginCal.getTime().toGMTString() + " - " + endCal.getTime().toGMTString());

            for (UsageStats app : queryUsageStats) {
                if (app.getPackageName().equals("com.facebook.katana")) {
                    String msg="App :" + app.getPackageName() + " | "
                            + "  | Total Time In Foreground :" + (float) (app.getTotalTimeInForeground() / 1000)
                            + "  | First TimeStamp :" + new Date(app.getFirstTimeStamp())
                            + "  | Last TimeStamp :" + new Date(app.getLastTimeStamp())
                            + "  | Last Time Used :" + new Date(app.getLastTimeUsed());
                    Log.d("tonmoy",msg);
                    accuracy_txt.setText(msg);
                }
                System.out.println("App :" + app.getPackageName() + " | "
                        + "  | Total Time In Foreground :" + (float) (app.getTotalTimeInForeground() / 1000)
                        + "  | First TimeStamp :" + new Date(app.getFirstTimeStamp())
                        + "  | Last TimeStamp :" + new Date(app.getLastTimeStamp())
                        + "  | Last Time Used :" + new Date(app.getLastTimeUsed()));
            }
        }

    }

}
