package com.tonmoy.healthdata;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;
import com.tonmoy.healthdata.model.AppSettings;
import com.tonmoy.healthdata.model.HealthInfo;
import com.tonmoy.healthdata.utility.UserCachedData;
import com.tonmoy.healthdata.utility.Utility;

import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class GetWearableDataService extends WearableListenerService {
    private File currentHealthFile, currentPPGFile;
    long prevTimestamp = 0;
    public static final int TYPE_PPG_SENSOR = 65545;

    @Override
    public void onCreate() {
        super.onCreate();
        currentHealthFile = Utility.getOutputFile(this, UserCachedData.getCachedData(this).app_settings.file_prefix_healthdata);
        currentPPGFile = Utility.getOutputFile(this, UserCachedData.getCachedData(this).app_settings.file_prefix_ppg);
    }

    HealthInfo healthInfo;

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        Log.d("tonmoy", "onDataChanged()");

        for (DataEvent dataEvent : dataEventBuffer) {
            if (dataEvent.getType() == DataEvent.TYPE_CHANGED) {
                DataItem dataItem = dataEvent.getDataItem();
                Uri uri = dataItem.getUri();
                String path = uri.getPath();

                if (path.startsWith("/sensors/")) {
                    unpackSensorData(
                            Integer.parseInt(uri.getLastPathSegment()),
                            DataMapItem.fromDataItem(dataItem).getDataMap()
                    );
                }
            }
        }
    }

    private void unpackSensorData(int msensorType, DataMap dataMap) {
        Log.d("tonmoy", "Size: " + dataMap.getDataMapArrayList("DATA").size());
        for (int i = 0; i < dataMap.getDataMapArrayList("DATA").size(); i++) {
            DataMap dataItemMap = dataMap.getDataMapArrayList("DATA").get(i);

            int accuracy = dataItemMap.getInt("ACCURACY");
            int sensorType = dataItemMap.getInt("SENSOR_TYPE");
            long timestamp = dataItemMap.getLong("TIMESTAMP");
            float[] values = dataItemMap.getFloatArray("VALUES");

            Log.d("tonmoy", "Received sensor data " + sensorType + " Main Value = " + dataItemMap.getFloat("MAIN_VALUE") + " ALL Values = " + Arrays.toString(values));
            SharedPreferences sharedPref = getSharedPreferences("HealthInfo", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("heartrate", (int) values[0]);
            editor.commit();
            //if (sensorType == Sensor.TYPE_HEART_RATE)
            //   Toast.makeText(this, "Heart Rate" + values[0], Toast.LENGTH_SHORT).show();
            // sensorManager.addSensorData(sensorType, accuracy, timestamp, values);
            // if(healthInfo==null )
            // {
            if (sensorType == TYPE_PPG_SENSOR) {
              //  startActivity(new Intent(GetWearableDataService.this,GraphPPGActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                sendMessageToActivity(timestamp, values);
                String ppg = toCSV(values);
                Log.d("PPG", ppg);
                storePPGData(timestamp + "", accuracy + "", ppg);
                continue;
            }
            HealthInfo healthInfo = new HealthInfo();
            healthInfo.timestamp = timestamp + "";
            // }
            switch (sensorType) {
                case Sensor.TYPE_HEART_RATE:
                    healthInfo.heartRateValue = values[0] + "";
                    healthInfo.heartAccuracy = accuracy + "";
                    break;
                case Sensor.TYPE_STEP_COUNTER:
                    healthInfo.stepCountValue = values[0] + "";
                    break;
                case Sensor.TYPE_LIGHT:
                    healthInfo.lightValue = values[0] + "";
                    break;
                default:
                    break;
            }
            storeData(healthInfo);
        }
    }


    private void storeData(HealthInfo healthData) {
        Log.d("tonmoy", "store Health data");
        if (currentHealthFile.length() > Utility.filesize) {
            currentHealthFile = createNewFile("Health_Data", AppSettings.FILE_PREFIX_HealthData);
        }
        String data = System.currentTimeMillis() + ","
                + healthData.timestamp + "," + healthData.heartRateValue + ","
                + healthData.heartAccuracy + "," + healthData.stepCountValue + ","
                + healthData.lightValue;
        Utility.writeStringAsFile(data, currentHealthFile, GetWearableDataService.this);
        Log.d("tonmoy", "size : " + currentHealthFile.length());
        Log.d("tonmoy", "write");
    }

    private void storePPGData(String timestamp, String accuracy, String PPGData) {
        Log.d("tonmoy", "store PPG data");
        if (currentPPGFile.length() > Utility.filesize) {
            currentPPGFile = createNewFile("PPG_Data", AppSettings.FILE_PREFIX_PPG);
        }
        String data = System.currentTimeMillis() + "," + timestamp + "," + accuracy + "," + PPGData;
        Utility.writeStringAsFile(data, currentPPGFile, GetWearableDataService.this);
        Log.d("tonmoy", "size : " + currentPPGFile.length());
        Log.d("tonmoy", "write");
    }

    public static String toCSV(float[] array) {
        String result = "";
        if (array.length > 0) {
            StringBuilder sb = new StringBuilder();
            for (float s : array) {
                sb.append(s).append(",");
            }
            result = sb.deleteCharAt(sb.length() - 1).toString();
        }
        return result;
    }

    private File createNewFile(String filePrefix, String key) {
        File file = null;
        SharedPreferences sharedPref = getSharedPreferences(
                "com.tonmoy.locationservice.utility", 0);

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String newFileName = filePrefix + timeStamp + ".txt";

        try {
            String data = sharedPref.getString("UserData", null);

            if (data != null) {
                JSONObject js = new JSONObject(data);
                js.put(key, newFileName);
                UserCachedData.saveCachedData(this, js.toString());
                file = Utility.getOutputFile(this, newFileName);
            }

        } catch (Exception e) {
            Log.d("Error", e.toString());
        }
        return file;
    }

    private void sendMessageToActivity(long timestamp,float ppg[]) {
        Intent intent = new Intent("intentKey");
// You can also include some extra data.
     //   intent.putExtra("key", msg);
        intent.putExtra("time", timestamp);
        intent.putExtra("ppg", ppg[0]);
        intent.putExtra("ppg1", ppg[1]);
        intent.putExtra("ppg2", ppg[2]);
        LocalBroadcastManager.getInstance(GetWearableDataService.this).sendBroadcast(intent);
    }

}
