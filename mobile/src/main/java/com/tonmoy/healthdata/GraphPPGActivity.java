package com.tonmoy.healthdata;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Random;

public class GraphPPGActivity extends AppCompatActivity {
    private final Handler mHandler = new Handler();
    //private Runnable mTimer1;
    private Runnable mTimer2;
    //  private LineGraphSeries<DataPoint> mSeries1;
    private LineGraphSeries<DataPoint> mSeries2;
    private double graph2LastXValue = 5d;
   // TextView tvStatus;
    float minPPG = Float.MAX_VALUE;
    float maxPPG = Float.MIN_VALUE;
    GraphView graph;
    ArrayList<Intent> data = new ArrayList<Intent>();
    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_ppg);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("intentKey"));
      //  tvStatus = (TextView) findViewById(R.id.textView);


        graph = (GraphView) findViewById(R.id.graph);
        mSeries2 = new LineGraphSeries<>();
        graph.addSeries(mSeries2);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(100);
        graph.getViewport().setMinY(1400000);
        graph.getViewport().setMaxY(1508567);
    }


    private DataPoint[] generateData() {
        int count = 30;
        DataPoint[] values = new DataPoint[count];
        for (int i = 0; i < count; i++) {
            double x = i;
            double f = mRand.nextDouble() * 0.15 + 0.3;
            double y = Math.sin(i * f + 2) + mRand.nextDouble() * 0.3;
            DataPoint v = new DataPoint(x, y);
            values[i] = v;
        }
        return values;
    }

    double mLastRandom = 2;
    Random mRand = new Random();

    private double getRandom() {
        return mLastRandom += mRand.nextDouble() * 0.5 - 0.25;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            // String message = intent.getStringExtra("key");
//            long time = intent.getLongExtra("time", 0);
//            float ppg = intent.getFloatExtra("ppg", 0);
//            if (counter % 400 == 0) {
//                minPPG = Float.MAX_VALUE;
//                maxPPG = Float.MIN_VALUE;
//            }
//            if (ppg > maxPPG) {
//                maxPPG = ppg;
//            }
//            if (ppg < minPPG) {
//                minPPG = ppg;
//            }
//            graph2.getViewport().setMinY(minPPG - 100);
//            graph2.getViewport().setMaxY(maxPPG + 100);
//            mSeries2.appendData(new DataPoint(counter++, ppg), true, 100);
            data.add(intent);
            if (data.size() > 100) {
                long minTime = Long.MAX_VALUE;
                float minPPG = Float.MAX_VALUE;
                long maxTime = Long.MIN_VALUE;
                float maxPPG = Float.MIN_VALUE;
                for (Intent i : data) {
                    float ppg = i.getFloatExtra("ppg", 0);
                    long time = i.getLongExtra("time", 0);
                    if (ppg > maxPPG) {
                        maxPPG = ppg;
                    }
                    if (ppg < minPPG) {
                        minPPG = ppg;
                    }
                    if (time > maxTime) {
                        maxTime = time;
                    }
                    if (time < minTime) {
                        minTime = time;
                    }
                }
                graph.getViewport().setMinY(minPPG - 100);
                graph.getViewport().setMaxY(maxPPG + 100);
                // graph2.getViewport().setMinX(minTime);
                // graph2.getViewport().setMaxX(maxTime);
                for (Intent i : data) {
                    long time = i.getLongExtra("time", 0);
                    float ppg = i.getFloatExtra("ppg", 0);
                    mSeries2.appendData(new DataPoint(counter++, ppg), true, 100);
                }
                data.clear();
            }
        }
    };
}
