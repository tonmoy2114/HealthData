package com.tonmoy.healthdata;

/*Developed by 
 Monsur Hossain Tonmoy 
 BUET 07
 eLITEs
 */

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.tonmoy.healthdata.model.AppSettings;
import com.tonmoy.healthdata.utility.PermissionUtils;
import com.tonmoy.healthdata.utility.SendLocationFetcherAsync;
import com.tonmoy.healthdata.utility.UserCachedData;
import com.tonmoy.healthdata.utility.Utility;

public class LocationService extends Service implements LocationListener, android.location.LocationListener,
		OnConnectionFailedListener, ConnectionCallbacks {
	public static final int MSG_REGISTER_CLIENT = 1;
	public static final int MSG_UNREGISTER_CLIENT = 2;
	public static final int MSG_SET_LAT_LONG_VALUE = 3;

	private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

	Handler handler;
	Runnable runable;

	private static int filesize = 1024 * 1024;

	private static boolean isRunning = false;

	private boolean isFirstTime;
	private File currentFile;

	private final Messenger mMessenger = new Messenger(
			new IncomingMessageHandler()); // Target we publish for clients to
	// send messages to IncomingHandler.

	private List<Messenger> mClients = new ArrayList<Messenger>(); // Keeps
	// track of
	// all
	// current
	// registered
	// clients.

	private GoogleApiClient mGoogleApiClient;
	private static final LocationRequest LOCATION_REQUEST = LocationRequest.create()
			.setInterval(1000)
			// 1 seconds
			.setFastestInterval(10)
			.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		isRunning = true;
		setUpLocationClientIfNeeded();
		mGoogleApiClient.connect();
		currentFile = Utility.getOutputFile(this);
		isFirstTime = true;
		// registerForScreenStateReceiver();
	}

	// private void registerForScreenStateReceiver() {
	// IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
	// filter.addAction(Intent.ACTION_SCREEN_OFF);
	// filter.addAction(Intent.ACTION_USER_PRESENT);
	// BroadcastReceiver mReceiver = new ScreenRecevier();
	// registerReceiver(mReceiver, filter);
	// }

	// @Override
	// public void onStart(Intent intent, int startId) {
	// boolean screenOn = intent.getBooleanExtra("screen_state", false);
	// if (!screenOn) {
	// Log.d("screenState", "On");
	// } else {
	// Log.d("screenState", "off");
	// }
	// }

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// boolean screenOn = intent.getBooleanExtra("screen_state", false);
		// if (!screenOn) {
		// Log.d("screenState", "On");
		// } else {
		// Log.d("screenState", "off");
		// }
		return START_STICKY; // Run until explicitly stopped.
	}

	@Override
	public IBinder onBind(Intent intent) {

		return mMessenger.getBinder();
	}

	private void setUpLocationClientIfNeeded() {
		if (mGoogleApiClient == null) {
			mGoogleApiClient = new GoogleApiClient.Builder(this, this, this).addApi(LocationServices.API).build(); // OnConnectionFailedListener//
			// ConnectionCallbacks

		}
	}

	@Override
	public void onLocationChanged(Location arg0) {
		if (isFirstTime) {
			isFirstTime = false;
			Log.d("tonmoy", "1st time");
			startDataCollect();

		}
		Log.d("Location",arg0.getAltitude()+"");
		sendMessageToUI(arg0);

	}

	@Override
	public void onStatusChanged(String s, int i, Bundle bundle) {

	}

	@Override
	public void onProviderEnabled(String s) {

	}

	@Override
	public void onProviderDisabled(String s) {

	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnected(Bundle arg0) {
		if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//			PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
//					android.Manifest.permission.ACCESS_FINE_LOCATION, true);
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			Log.d("tonmoy","No permission");
			return;
		}
		LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
				LOCATION_REQUEST, this);
		Log.d("tonmoy","LOCATION REQUESTED");
		LocationManager locationManager = (LocationManager)
				getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, this);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 10, this);

	}

	@Override
	public void onConnectionSuspended(int i) {

	}


	/**
	 * Handle incoming messages from MainActivity
	 */
	private class IncomingMessageHandler extends Handler { // Handler of
		// incoming messages
		// from clients.
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case MSG_REGISTER_CLIENT:
					mClients.add(msg.replyTo);
					break;
				case MSG_UNREGISTER_CLIENT:
					mClients.remove(msg.replyTo);
					break;

				default:
					super.handleMessage(msg);
			}
		}
	}

	public static boolean isRunning() {
		return isRunning;
	}

	/**
	 * Send the data to all clients.
	 *
	 */
	private void sendMessageToUI(Location arg0) {
		Iterator<Messenger> messengerIterator = mClients.iterator();
		while (messengerIterator.hasNext()) {
			Messenger messenger = messengerIterator.next();
			try {
				Bundle bundle = new Bundle();
				bundle.putParcelable("location", arg0);
				Message msg = Message.obtain(null, MSG_SET_LAT_LONG_VALUE);
				msg.setData(bundle);
				messenger.send(msg);

			} catch (RemoteException e) {
				// The client is dead. Remove it from the list.
				mClients.remove(messenger);
			}
		}
	}

	private void startDataCollect() {
		handler = new Handler();
		runable = new Runnable() {
			@Override
			public void run() {
				try {
					if (UserCachedData.getCachedData(LocationService.this).app_settings.isLocationStorageEnabled) {
						storeData();
					}
					if (UserCachedData.getCachedData(LocationService.this).app_settings.dataTransmitEnabled) {
						sendData();
					}
				} catch (Exception e) {
					// TODO: handle exception
				} finally {
					handler.postDelayed(
							this,
							UserCachedData.getCachedData(LocationService.this).app_settings.updateFrequency * 1000);
				}
			}

		};
		if (UserCachedData.getCachedData(LocationService.this).app_settings.isLocationStorageEnabled) {
			storeData();
		}
		if (UserCachedData.getCachedData(LocationService.this).app_settings.dataTransmitEnabled) {
			sendData();
		}
		handler.postDelayed(
				runable,
				UserCachedData.getCachedData(this).app_settings.updateFrequency * 1000);
	}

	private void sendData() {
//		String url = UserCachedData.getCachedData(this).app_settings.url;
//		Log.d("tonmoy", url);
//		Location loc = mGoogleApiClient.getLastLocation();
//		if (loc == null)
//			return;
//		String data = UserCachedData.getCachedData(this).app_settings.rover_id
//				+ "," + System.currentTimeMillis() / 1000L + ","
//				+ loc.getLatitude() + "," + loc.getLongitude() + ","
//				+ loc.getAltitude() + "," + loc.getSpeed() + ","
//				+ loc.getAccuracy() + "\n";
//
//		SendLocationFetcherAsync fAsync = new SendLocationFetcherAsync(url,
//				data);
//		fAsync.execute();

	}

	private void storeData() {
		Log.d("tonmoy", "store data");
		if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			return;
		}
		Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if (loc == null)
			return;
		if (currentFile.length() > filesize) {
			createNewFile();
		}
		String data = loc.getTime() + "," +loc.getLatitude() + "," + loc.getLongitude() + ","
				+ loc.getAltitude() + "," + loc.getSpeed() + ","
				+ loc.getAccuracy();
		Utility.writeStringAsFile(data, currentFile, LocationService.this);
		Log.d("tonmoy", "size : " + currentFile.length());
		Log.d("tonmoy", "write");
	}

	private void createNewFile() {
		SharedPreferences sharedPref = getSharedPreferences(
				"com.tonmoy.locationservice.utility", 0);

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String newFileName = "Location_Data" + timeStamp + ".txt";

		try {
			String data = sharedPref.getString("UserData", null);

			if (data != null) {
				JSONObject js = new JSONObject(data);
				js.put(AppSettings.FILE_PREFIX, newFileName);
				UserCachedData.saveCachedData(this, js.toString());
				currentFile = Utility.getOutputFile(this, newFileName);
			}

		} catch (Exception e) {
			Log.d("Error", e.toString());
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mGoogleApiClient != null) {
			mGoogleApiClient.disconnect();
		}

		Log.i("MyService", "Service Stopped.");
		isRunning = false;
		if (handler != null) {
			handler.removeCallbacks(runable);
		}
	}

}
