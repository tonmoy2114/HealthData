package com.tonmoy.healthdata;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    Button buttonStressed, buttondLocationManagement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        buttondLocationManagement = (Button) findViewById(R.id.buttonLocationManagement);
        buttonStressed = (Button) findViewById(R.id.buttonStressed);

        buttondLocationManagement.setOnClickListener(this);
        buttonStressed.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == buttondLocationManagement) {
            Intent intent = new Intent(this, LocationManagementActivity.class);
            startActivity(intent);
        } else if (view == buttonStressed) {
            final String []stressFellings=new String[] {"Very Stressed","Stressed","Moderate", "Not Sure"};
            AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
            //alt_bld.setIcon(R.drawable.icon);
            alt_bld.setTitle("How Stressed are you right now?");
            alt_bld.setSingleChoiceItems(stressFellings, -1, new DialogInterface
                    .OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    Toast.makeText(getApplicationContext(),
                            "Group Name = " + stressFellings[item], Toast.LENGTH_SHORT).show();
                    dialog.dismiss();

                }
            });
            AlertDialog alert = alt_bld.create();
            alert.show();
        }
    }
}
