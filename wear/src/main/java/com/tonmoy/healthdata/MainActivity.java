package com.tonmoy.healthdata;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends Activity implements SensorEventListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, DataApi.DataListener {

    private static final String TAG = "MainActivity";
    private TextView mTextViewStepCount;
    private TextView mTextViewHeartRate;
    private TextView mTextViewLight;
    private Button buttonStop;
    private TextView mTextViewLog;
    // int heartBeat = 0;
    private GoogleApiClient mGoogleApiClient;
    private static final int CLIENT_CONNECTION_TIMEOUT = 15000;
    public static final int TYPE_PPG_SENSOR = 65545;
    public static final int TYPE_HEART_RATE_TEST_SENSOR = 65547;
    public static final int TYPE_WELLNESS_PASSIVE_SENSOR = 65538;

    SensorManager mSensorManager;

    ArrayList<DataMap> allData=new ArrayList<DataMap>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Keep the Wear screen always on (for testing only!)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextViewStepCount = (TextView) stub.findViewById(R.id.step_count);
                mTextViewHeartRate = (TextView) stub.findViewById(R.id.heartRate);
                mTextViewLight = (TextView) stub.findViewById(R.id.light);
                mTextViewLog = (TextView) stub.findViewById(R.id.detailsLog);
                buttonStop = (Button) stub.findViewById(R.id.buttonStop);
                buttonStop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Wearable.DataApi.removeListener(mGoogleApiClient, MainActivity.this);
                        mGoogleApiClient.disconnect();
                        mSensorManager.unregisterListener(MainActivity.this);
                    }
                });

                mTextViewHeartRate.setText("No Heart Rate");
                mTextViewLight.setText("No Light");
                mTextViewStepCount.setText("No Step counter");
                getStepCount();
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        // mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    private void getStepCount() {
        mSensorManager = ((SensorManager) getSystemService(SENSOR_SERVICE));
        List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        for (Sensor msensor : sensors) {
            Log.d("tonmoy", msensor.toString());
        }

        Sensor mStepCountSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        Sensor mHeartRateSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        Sensor mPPG = mSensorManager.getDefaultSensor(TYPE_PPG_SENSOR);
        Sensor mLightSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

      //  mSensorManager.registerListener(this, mStepCountSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mHeartRateSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mPPG, SensorManager.SENSOR_DELAY_NORMAL);
       // mSensorManager.registerListener(this, mLightSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private String currentTimeStr() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        return df.format(c.getTime());
    }

//    public void onAccuracyChanged(Sensor sensor, int accuracy) {
//        Log.d(TAG, "onAccuracyChanged - accuracy: " + accuracy);
//    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        String text = "";
        if (sensorEvent.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            text = "StepCount";
            String msg = "Count: " + (int) sensorEvent.values[0];
            mTextViewStepCount.setText(msg);
            // Log.d("tonmoy", msg);
        } else if (sensorEvent.sensor.getType() == TYPE_WELLNESS_PASSIVE_SENSOR) {
            text = "TYPE_WELLNESS_PASSIVE_SENSOR";
            String msg = "TYPE_WELLNESS_PASSIVE_SENSOR: " + Arrays.toString(sensorEvent.values) + " time" + sensorEvent.timestamp;
          //  Log.d("tonmoy", msg);
        } else if (sensorEvent.sensor.getType() == TYPE_PPG_SENSOR) {
            text = "TYPE_PPG_SENSOR";
            String msg = "TYPE_PPG_SENSOR: " + Arrays.toString(sensorEvent.values) + " time" + sensorEvent.timestamp;
            Log.d("tonmoy", msg);
        } else if (sensorEvent.sensor.getType() == Sensor.TYPE_HEART_RATE) {
            text = "HeartRate Detected";
            String msg = "Heart Rate: " + (int) sensorEvent.values[0] + " AC" + sensorEvent.accuracy;
            mTextViewHeartRate.setText(msg);
            Log.d("tonmoy", msg);
        } else if (sensorEvent.sensor.getType() == Sensor.TYPE_LIGHT) {
            text = "Light ";
            String msg = "Light :" + sensorEvent.values[0];
            mTextViewLight.setText(msg);
            //  Log.d("tonmoy", msg);
        } else
            Log.d(TAG, "Unknown sensor type " + sensorEvent.sensor.getType() + " Name: " + sensorEvent.sensor.getName());

        //  mTextViewLog.setText("Log :" + text);
        sendSensorDataInBackground(sensorEvent.sensor.getType(), sensorEvent.accuracy, sensorEvent.timestamp, sensorEvent.values);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Wearable.DataApi.addListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void sendSensorDataInBackground(int sensorType, int accuracy, long timestamp, float[] values) {

        //PutDataMapRequest dataMap = PutDataMapRequest.create("/sensors/" + sensorType);

      //  dataMap.getDataMap().putInt("ACCURACY", accuracy);
       // dataMap.getDataMap().putInt("ACCURACY", counter);
      //  dataMap.getDataMap().putLong("TIMESTAMP", timestamp);
       // dataMap.getDataMap().putLong("ExtraTIMESTAMP", System.currentTimeMillis());
       // dataMap.getDataMap().putFloatArray("VALUES", values);
       // dataMap.getDataMap().putFloat("PPG", values[0]);

        DataMap item= new DataMap();
        item.putInt("ACCURACY", accuracy);
        item.putInt("SENSOR_TYPE", sensorType);
        item.putLong("TIMESTAMP", timestamp);
        item.putFloat("MAIN_VALUE", values[0]);
        item.putFloatArray("VALUES", values.clone());
        allData.add(item);
        if(allData.size()>50)
        {
            PutDataMapRequest dataMap = PutDataMapRequest.create("/sensors/" + sensorType);
            dataMap.getDataMap().putDataMapArrayList("DATA", allData);
            dataMap.setUrgent();
            PutDataRequest putDataRequest = dataMap.asPutDataRequest();
            send(putDataRequest);
            allData.clear();
        }
        //PutDataRequest putDataRequest = dataMap.asPutDataRequest();
       // send(putDataRequest);
    }

    private boolean validateConnection() {
        if (mGoogleApiClient.isConnected()) {
            return true;
        }

        ConnectionResult result = mGoogleApiClient.blockingConnect(CLIENT_CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);

        return result.isSuccess();
    }

    private void send(PutDataRequest putDataRequest) {
        Wearable.DataApi.putDataItem(mGoogleApiClient, putDataRequest)
                .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                    @Override
                    public void onResult(DataApi.DataItemResult dataItemResult) {
                        Log.d("tonmoy", "Sending sensor data: " + dataItemResult.getStatus().isSuccess());
                    //    dataItemResult.getDataItem().
                    }
                });
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {

    }
}